const express = require('express');
const router = express.Router();
const connection = require('./db');


router.get('/data', (req, res) => {
  connection.query('SELECT * FROM user_data', (err, results) => {
    if (err) throw err;
    res.json(results);
  });
});

router.post('/data', (req, res) => {
  const { name, studentNumber, course, height, weight, age, symptoms, potentialIssues, submissionDateTime } = req.body;

  if (!name || !studentNumber || !course || !height || !weight || !age || !symptoms || !potentialIssues || !submissionDateTime) {
    return res.status(400).json({ error: 'Incomplete data. Please provide all required fields.' });
  }

  const sql = 'INSERT INTO user_data (name, student_number, course, height, weight, age, symptoms, potential_issues, submission_date_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW())';
  const values = [name, studentNumber, course, height, weight, age, JSON.stringify(symptoms), JSON.stringify(potentialIssues), submissionDateTime];

  connection.query(sql, values, (err, results) => {
    if (err) {
      console.error('Error inserting user data:', err);
      res.status(500).json({ error: 'Error inserting user data' });
    } else {
      res.status(200).json({ message: 'User data submitted successfully' });
    }
  });
});


router.put('/data/:id', (req, res) => {
  const id = req.params.id;
  const { name, studentNumber, course, symptoms, potentialIssues } = req.body;

  const sql = 'UPDATE user_data SET name=?, student_number=?, course=?, symptoms=?, potential_issues=? WHERE id=?';
  const values = [name, studentNumber, course, JSON.stringify(symptoms), JSON.stringify(potentialIssues), id];

  connection.query(sql, values, (err, results) => {
    if (err) {
      console.error('Error updating user data:', err);
      res.status(500).json({ error: 'Error updating user data' });
    } else {
      res.status(200).json({ message: 'User data updated successfully' });
    }
  });
});

router.delete('/data/:id', (req, res) => {
  const id = req.params.id;

  const sql = 'DELETE FROM user_data WHERE id=?';
  const values = [id];

  connection.query(sql, values, (err, results) => {
    if (err) {
      console.error('Error deleting user data:', err);
      res.status(500).json({ error: 'Error deleting user data' });
    } else {
      res.status(200).json({ message: 'User data deleted successfully' });
    }
  });
});

module.exports = router;