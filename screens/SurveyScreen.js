import React, { useState } from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet } from 'react-native';
import CheckBox from 'react-native-check-box';
import symptoms from './symptoms';
import symptomToIssueMapping from './symptomToIssueMapping';
import SecurityForm from './securityForm';
import axios from 'axios';
import moment from 'moment-timezone';

export default function SurveyScreen() {
  const [selectedSymptoms, setSelectedSymptoms] = useState([]);
  const [potentialIssues, setPotentialIssues] = useState([]);
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [securityFormDetails, setSecurityFormDetails] = useState(null);
  const [course, setCourse] = useState('');
  const [showConfirmation, setShowConfirmation] = useState(false);

  const handleDetailsSubmit = (details) => {
    setSecurityFormDetails(details);
    setCourse(details.course);
  }

  const handleSymptomChange = (symptom) => {
    const updatedSymptoms = selectedSymptoms.includes(symptom)
      ? selectedSymptoms.filter((s) => s !== symptom)
      : [...selectedSymptoms, symptom];
    setSelectedSymptoms(updatedSymptoms);
  };

const handleSubmit = async () => {
  if (!securityFormDetails) {
    console.error('Please submit security form details first.');
    return;
  }

  const potentialIssues = getPotentialIssues(selectedSymptoms);
  setPotentialIssues(potentialIssues);

  setShowConfirmation(true);
};

const handleSubmissionConfirmed = async () => {
  setShowConfirmation(false);

  const formattedDateTime = moment().tz('Asia/Manila').format('YYYY-MM-DD HH:mm:ss');

  try {
    const response = await axios.post('http://192.168.100.14:2005/api/data', {
      name: securityFormDetails.name,
      studentNumber: securityFormDetails.studentNumber,
      course: securityFormDetails.course,
      height: securityFormDetails.height,
      weight: securityFormDetails.weight,
      age: securityFormDetails.age,
      symptoms: selectedSymptoms,
      potentialIssues: potentialIssues,
      submissionDateTime: formattedDateTime,
    });

    if (response.status === 200) {
      console.log('User data submitted successfully');
      setIsPopupOpen(true);
    } else {
      console.error('Error submitting user data:', response.data.error);
    }
  } catch (error) {
    console.error('Error submitting user data:', error);
  }
};

  const handleSubmissionCancelled = () => {
    setShowConfirmation(false);
  };

  const handleReturnToForm = () => {
    setIsPopupOpen(false);
    setSelectedSymptoms([]);
    setPotentialIssues([]);
    setSecurityFormDetails(null);
  };

  const getPotentialIssues = (selectedSymptoms) => {
    const potentialIssues = selectedSymptoms.reduce((issues, symptom) => {
      if (symptomToIssueMapping[symptom]) {
        issues.push(...symptomToIssueMapping[symptom]);
      }
      return issues;
    }, []);

    return [...new Set(potentialIssues)];
  };

  return (
    <View style={styles.surveyContainer}>
      {securityFormDetails ? (
        <ScrollView style={{ ...styles.surveyForm, marginTop: 30, marginBottom: 60, }}>
          <Text style={styles.formHeading}>Choose symptoms:</Text>
          {symptoms.map((symptom) => (
  <View key={symptom} style={styles.symptomContainer}>
    <View style={styles.checkboxContainer}>
      <CheckBox isChecked={selectedSymptoms.includes(symptom)} onClick={() => handleSymptomChange(symptom)} />
      <Text style={styles.checkboxLabel}>{symptom}</Text>
    </View>
  </View>
))}
          <TouchableOpacity style={styles.submitButton} onPress={handleSubmit}>
            <Text style={styles.submitButtonText}>Submit</Text>
          </TouchableOpacity>
        </ScrollView>
      ) : (
        <SecurityForm onDetailsSubmit={handleDetailsSubmit} />
      )}

      {showConfirmation && (
        <View style={styles.popup}>
          <View style={styles.popupContent}>
            <Text style={styles.confirmationText}>Are you sure you want to submit with the following symptoms?</Text>
            <ScrollView style={styles.potentialIssuesList}>
              {selectedSymptoms.map((symptom, index) => (
                <Text key={index}>{symptom}</Text>
              ))}
            </ScrollView>
            <View style={styles.confirmationButtons}>
              <TouchableOpacity style={styles.confirmButton} onPress={handleSubmissionConfirmed}>
                <Text style={styles.confirmButtonText}>Yes</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.cancelButton} onPress={handleSubmissionCancelled}>
                <Text style={styles.cancelButtonText}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}

      {isPopupOpen && (
        <View style={styles.popup}>
          <View style={styles.popupContent}>
            <TouchableOpacity style={styles.close} onPress={handleReturnToForm}>
              <Text style={styles.closeText}>&times;</Text>
            </TouchableOpacity>
            <Text style={styles.potentialIssuesHeading}>Potential Health Issues:</Text>
            <ScrollView style={styles.potentialIssuesList}>
              {potentialIssues.map((issue, index) => (
                <Text key={index}>{issue}</Text>
              ))}
            </ScrollView>
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  surveyContainer: {
    flex: 1,
    backgroundColor: 'lightblue',
    padding: 20,
  },
  surveyForm: {
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 10,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  formHeading: {
    fontSize: 25,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  checkboxContainer: {
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: 10,
},
checkbox: {
  width: 20,
  height: 20,
  borderWidth: 2,
  borderColor: '#3CB371',
  borderRadius: 3,
},
checkboxLabel: {
  marginLeft: 10,
  fontSize: 25,
  flex: 1,
},
symptomContainer: {
  borderWidth: 2,
  borderColor: '#000',
  borderRadius: 5,
  marginBottom: 4,
},
submitButton: {
  backgroundColor: '#3CB371',
  padding: 20,
  width: 200,
  borderRadius: 10,
  alignItems: 'center',
  alignSelf: 'center', 
  marginTop: 20, 
},
  submitButtonText: {
    color: 'white',
    fontSize: 16,
  },
  popup: {
    position: 'absolute',
    top: 0,
    left: 20,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  popupContent: {
    backgroundColor: 'white',
    padding: 25,
    borderRadius: 10,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  close: {
    position: 'absolute',
    top: 10,
    right: 10,
    fontSize: 24,
  },
  closeText: {
    fontSize: 30,
    color: 'black',
  },
  potentialIssuesHeading: {
    fontSize: 30,
    marginBottom: 15,
  },
  potentialIssuesList: {
    maxHeight: 150,
  },
  confirmationText: {
    fontSize: 18,
    marginBottom: 10,
  },
  confirmationButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 10,
  },
  confirmButton: {
    backgroundColor: '#3CB371',
    padding: 10,
    borderRadius: 5,
  },
  confirmButtonText: {
    color: 'white',
    fontSize: 16,
  },
  cancelButton: {
    backgroundColor: 'gray',
    padding: 10,
    borderRadius: 5,
  },
  cancelButtonText: {
    color: 'white',
    fontSize: 14,
  },
});
