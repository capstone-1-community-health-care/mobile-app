import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function HomeScreen({ navigation }) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <Image
          source={require("../assets/images/home.png")}
          style={[styles.logo, styles.watermark]}
        />
        <Text style={styles.title}>
          Welcome to CommuniCare
        </Text>
        <Text style={styles.subtitle}>
          Your Health, Our Passion
        </Text>
        <Text style={styles.paragraph}>
          "Whether you're proactively managing your mental health, seeking relief from headaches, or looking to improve your cognitive function, 
          CommuniCare is your trusted partner on your journey to better health.
           We provide comprehensive support and resources to help you prioritize and maintain your mental and cognitive well-being."
        </Text>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => navigation.navigate('Survey')}
        >
          <Text style={styles.btnText}>Take Survey</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightblue',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  logo: {
    width: 250,  
    height: 250,  
    resizeMode: 'contain',  
    marginBottom: 20,
  },
  watermark: {
    opacity: 0.9,
  },
  title: {
    fontSize: 32,
    color: '#000',
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  subtitle: {
    fontSize: 24,
    color: '#000',
    marginBottom: 20,
    textAlign: 'center',
  },
  paragraph: {
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
    marginBottom: 30,
  },
  btn: {
    backgroundColor: '#274a8d',
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderRadius: 5,
  },
  btnText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
  },
});
