import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import { themeColors } from '../theme'
import { useNavigation } from '@react-navigation/native'

export default function WelcomeScreen() {
    const navigation = useNavigation();

    return (
        <SafeAreaView className="flex-1" style={{ backgroundColor: themeColors.secondary }}>
            <View className="flex-row justify-center">
                <Image source={require("../assets/images/welcomehero2.png")}
                    style={{
                        height: 300,
                        width: 300,
                        borderRadius: 20,
                        position: "relative",
                        top: 10,
                        left: 45,
                        transform: [
                            { translateX: -45 },
                            { translateY: 50 },
                        ]
                    }} />
            </View>
            <View className="flex-1 flex justify-around my-10">
                <Text
                    className="text-black font-bold text-4xl text-center">
                    CommuniCare
                </Text>
                <View style={{ marginVertical: 10 }}>
                    <Text style={{
                        fontSize: 16,
                        color: themeColors.black,
                        textAlign: 'center',
                    }}>We are here to serve you better</Text>
                    <Text style={{
                        fontSize: 16,
                        color: themeColors.black,
                        textAlign: 'center',
                    }}>We care about your health and hope to help you.</Text>
                </View>
                <View className="space-y-4">
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Login')}
                        style={{ backgroundColor: '#70bb85', paddingVertical: 12, marginHorizontal: 7, borderRadius: 10 }}
                    >
                        <Text
                            style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}
                        >
                            Let's Get Started
                        </Text>
                    </TouchableOpacity>
                    <View className="flex-row justify-center">
                        <Text className="text-black font-semibold">Already have an account?</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                            <Text style={{ color: '#70bb85', fontWeight: 'bold' }}> SignUp</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}
