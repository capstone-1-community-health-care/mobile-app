import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const Guidelines = () => {
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.guidelineContainer}>
        <Text style={styles.header}>Home</Text>
        <Text style={styles.guidelineText}>
          Access Homepage and can directly go to Surveypage using Take survey button.
        </Text>
      </View>

      <View style={styles.guidelineContainer}>
        <Text style={styles.header}>Profile</Text>
        <Text style={styles.guidelineText}>
          View your information and edit it.
        </Text>
        <Text style={styles.guidelineText}>
          Functionality:
            - View your personal information.
            - Edit your information and add a photo.
        </Text>
        <Text style={styles.guidelineText}>
          Description: You can access and review your personal details stored in the application. Additionally, edit your profile information, including adding or updating your photo to personalize your profile.
        </Text>
      </View>

      <View style={styles.guidelineContainer}>
        <Text style={styles.header}>Chat System</Text>
        <Text style={styles.guidelineText}>
          Consult with our nurse campus through the chat system.
        </Text>
        <Text style={styles.guidelineText}>
          Functionality: Initiate a conversation with our nurse campus for healthcare consultations.
        </Text>
        <Text style={styles.guidelineText}>
          Description: Utilize the chat system to communicate with our nurse campus for any healthcare-related inquiries or consultations. Seek advice, ask questions, and receive guidance from our healthcare professionals.
        </Text>
      </View>

      <View style={styles.guidelineContainer}>
        <Text style={styles.header}>Survey Page</Text>
        <Text style={styles.guidelineText}>
          Fill out a survey to assess your symptoms.
        </Text>
        <Text style={styles.guidelineText}>
          Functionality: Complete a survey to evaluate your symptoms and health condition.
        </Text>
        <Text style={styles.guidelineText}>
          Description: Access the survey page to provide information about your symptoms and health status. The survey aims to help you assess your well-being and provide recommendations based on your responses.
        </Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 50,
    backgroundColor: 'lightblue'
  },
  guidelineContainer: {
    marginBottom: 20,
  },
  header: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    color: 'black'
  },
  guidelineText: {
    fontSize: 16,
    marginBottom: 5,
    color: 'black'
  },
});

export default Guidelines;
