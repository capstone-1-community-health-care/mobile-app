import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  SafeAreaView,
  Button,
  TextInput,
  FlatList,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  TouchableWithoutFeedback,
  BackHandler,
} from 'react-native';
import { collection, addDoc, serverTimestamp, onSnapshot, query, orderBy, limit, getDocs, setDoc, doc } from 'firebase/firestore';
import { firestore } from '../config/firebase';
import { getAuth } from 'firebase/auth';
import Constants from 'expo-constants';
import { Ionicons } from '@expo/vector-icons';

const ChatScreen = () => {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const [selectedMessage, setSelectedMessage] = useState(null);
  const messageListRef = useRef(null);
  const [searchText, setSearchText] = useState('');
  const [isConversationRead, setIsConversationRead] = useState(false);

  const auth = getAuth();
  const user = auth.currentUser;

  const currentUser = user
    ? {
        displayName: user.displayName,
        photoURL: user.photoURL,
        uid: user.uid,
        specialty: user.specialty || 'Student',
      }
    : {
        displayName: 'Anonymous',
        photoURL: null,
        specialty: 'Student',
      };

  const getConversationId = (uid1, uid2) => {
    return [uid1, uid2].sort().join('_');
  };

  useEffect(() => {
    const backAction = () => {
      if (selectedUser) {
        setSelectedUser(null);
        return true; 
      }
      return false;
    };

    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);

    return () => backHandler.remove();
  }, [selectedUser]);
  
  useEffect(() => {
    // Add logic to check if conversation is read
    const conversationIsRead = true; // Add your logic here to determine if conversation is read
    setIsConversationRead(conversationIsRead);
  }, []);

  useEffect(() => {
    const fetchLastMessages = async () => {
      const conversationsRef = collection(firestore, 'conversations');
      const promises = admins.map(async (admin) => {
        const conversationId = getConversationId(currentUser.uid, admin.uid);
        const messagesCollection = collection(conversationsRef, conversationId, 'messages');
        const lastMessageQuery = query(messagesCollection, orderBy('timestamp', 'desc'), limit(1));
        const querySnapshot = await getDocs(lastMessageQuery);
        const lastMessageDoc = querySnapshot.docs[0];
        const lastMessage = lastMessageDoc ? lastMessageDoc.data() : null;
        admin.lastMessage = lastMessage;
        return admin;
      });
      const updatedAdmins = await Promise.all(promises);
      // Sort admins based on last message timestamp
      updatedAdmins.sort((a, b) => {
        const timeA = a.lastMessage ? a.lastMessage.timestamp?.toMillis() : 0;
        const timeB = b.lastMessage ? b.lastMessage.timestamp?.toMillis() : 0;
        return timeB - timeA; // Sort in descending order
      });
      setAdmins(updatedAdmins);
    };

    fetchLastMessages();

    // Set up snapshot listener for messages of each conversation
    const unsubscribeListeners = admins.map((admin) => {
      const conversationId = getConversationId(currentUser.uid, admin.uid);
      const messagesCollection = collection(firestore, 'conversations', conversationId, 'messages');
      return onSnapshot(messagesCollection, () => {
        fetchLastMessages();
      });
    });

    return () => {
      unsubscribeListeners.forEach((unsubscribe) => unsubscribe());
    };
  }, [currentUser.uid, admins]);

  useEffect(() => {
  const adminsCollection = collection(firestore, 'admins');

  const unsubscribeAdmins = onSnapshot(adminsCollection, (querySnapshot) => {
    const updatedAdmins = querySnapshot.docs
  .map((doc) => ({
    uid: doc.id,
    ...doc.data(),
  }))
  .filter((admin) => admin.uid !== currentUser.uid);

    setAdmins(updatedAdmins);
  });

  return () => {
    unsubscribeAdmins();
  };
}, [currentUser.uid]);

  // Add a new state for admins
  const [admins, setAdmins] = useState([]);

  const handleAdminClick = async (admin) => {
    setSelectedUser(admin);
    setSearchText('');

    const conversationId = getConversationId(currentUser.uid, admin.uid);
    const messagesCollection = collection(
      firestore,
      'conversations',
      conversationId,
      'messages'
    );
    const lastMessageQuery = query(
      messagesCollection,
      orderBy('timestamp', 'desc'),
      limit(1)
    );

    const querySnapshot = await getDocs(lastMessageQuery);
    const lastMessageDoc = querySnapshot.docs[0];
    const lastMessage = lastMessageDoc ? lastMessageDoc.data() : null;


    // Update the state with the modified admin object
    setAdmins((prevAdmins) =>
      prevAdmins.map((a) =>
        a.uid === admin.uid ? { ...a, lastMessage } : a
      )
    );
  };

  useEffect(() => {
    if (selectedUser) {
      const messagesCollection = collection(
        firestore,
        'conversations',
        getConversationId(currentUser.uid, selectedUser.uid),
        'messages'
      );

      const unsubscribeMessages = onSnapshot(messagesCollection, (querySnapshot) => {
        const updatedMessages = querySnapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));

        updatedMessages.sort((a, b) => {
          const timeA = a.timestamp ? a.timestamp.toMillis() : 0;
          const timeB = b.timestamp ? b.timestamp.toMillis() : 0;
          return timeA - timeB;
        });

        setMessages(updatedMessages);

        const updatedAdmins = admins.map((admin) => {
          if (admin.uid === selectedUser.uid) {
            return {
              ...admin,
              lastMessage: updatedMessages.length > 0 ? updatedMessages[updatedMessages.length - 1] : null,
            };
          }
          return admin;
        });
        setAdmins(updatedAdmins);
      });

      return () => {
        unsubscribeMessages();
      };
    }
  }, [selectedUser, currentUser.uid, admins]);

  const scrollToBottom = () => {
    if (messageListRef.current) {
      messageListRef.current.scrollToEnd({ animated:true });
    }
  };

  const handleSendMessage = async () => {
    if (newMessage.trim() === '' || !selectedUser) return;

    try {
      const conversationId = getConversationId(currentUser.uid, selectedUser.uid);

      // Add message to conversation messages collection
      const messageRef = await addDoc(
        collection(
          firestore,
          'conversations',
          conversationId,
          'messages'
        ),
        {
          text: newMessage,
          timestamp: serverTimestamp(),
          displayName: currentUser.displayName,
          photoURL: currentUser.photoURL,
          uid: currentUser.uid,
          specialty: currentUser.specialty,
        }
      );

      // Update last message for the current user in the subcollection
      await setDoc(
        doc(
          firestore,
          'conversations',
          conversationId,
          'lastMessages',
          currentUser.uid
        ),
        {
          text: newMessage,
          timestamp: serverTimestamp(),
          displayName: currentUser.displayName,
          photoURL: currentUser.photoURL,
          uid: currentUser.uid,
          specialty: currentUser.specialty,
        }
      );

      const message = {
        id: messageRef.id,
        text: newMessage,
        displayName: currentUser.displayName,
        photoURL: currentUser.photoURL || 'URL_TO_DEFAULT_AVATAR_IMAGE',
        uid: currentUser.uid,
        timestamp: new Date(),
        specialty: currentUser.specialty,
      };

      setNewMessage('');
      scrollToBottom();
    } catch (error) {
      console.error('Error sending message: ', error);
    }
  };

  const handleMessageClick = (message) => {
    setSelectedMessage(selectedMessage === message ? null : message);
  };

  const handleUserClick = (user) => {
    setSelectedUser(user);
    setSearchText(''); // Reset search text when user is selected
  };

  const handleBackButtonClick = () => {
    setSelectedUser(null);
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.keyboardAvoidingContainer}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 100 : 0}
      >
        <View style={[styles.chatContainer, { paddingBottom: Constants.statusBarHeight + 105 }]}>
          {selectedUser ? (
            <>
              <Text style={styles.title2}>Consultation</Text>
              <TouchableWithoutFeedback onPress={handleBackButtonClick} style={styles.backButton}>
                <Ionicons name="arrow-back" size={30} color="#007BFF" style={styles.backIcon} />
              </TouchableWithoutFeedback>
              <FlatList
                style={styles.messageList}
                data={messages}
                keyExtractor={(message) => message.id}
                renderItem={({ item: message }) => (
                  <View
                    style={[
                      styles.message,
                      message.uid === currentUser.uid ? styles.sentMessage : styles.receivedMessage,
                    ]}
                    onTouchEnd={() => handleMessageClick(message)}
                  >
                    <TouchableWithoutFeedback onPress={() => handleMessageClick(message)}>
                      <View>
                        <View style={styles.messageHeader}>
                          <Image source={{ uri: message.photoURL || 'URL_TO_DEFAULT_AVATAR_IMAGE' }} style={styles.messageAvatar} />
                          <Text style={styles.messageSender}>{message.displayName}</Text>
                          {message.specialty && message.specialty.trim() !== '' && (
                            <View style={styles.specialtyBubble}>
                              <Text style={styles.specialtyText}>{message.specialty}</Text>
                            </View>
                          )}
                        </View>
                        <Text style={styles.messageText} textAlign="justify">
                          {message.text}
                        </Text>
                        {selectedMessage && selectedMessage.id === message.id && (
                          <Text style={styles.messageDetails}>
                            {message.timestamp ? (
                              `Timestamp: ${message.timestamp.toDate().toLocaleString()}`
                            ) : (
                              'No timestamp available'
                            )}
                          </Text>
                        )}
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                )}
                ref={messageListRef}
                onContentSizeChange={scrollToBottom}
                keyboardShouldPersistTaps="handled"
              />
              <View style={styles.chatInput}>
                <TextInput
                  style={styles.input}
                  placeholder="Type a message..."
                  placeholderTextColor="#000"
                  value={newMessage}
                  onChangeText={(text) => setNewMessage(text)}
                  onSubmitEditing={handleSendMessage}
                />
                <TouchableOpacity onPress={handleSendMessage} style={styles.button} activeOpacity={0.7}>
                <Ionicons name="send" size={30} color="black" />
                  </TouchableOpacity>
              </View>
            </>
          ) : (
            <View style={styles.userList}>
            <Text style={styles.title3}>Chat</Text>
              <View style={styles.searchBar}>
                <Ionicons name="search" size={24} color="black" style={styles.searchIcon} />
                <TextInput
                  style={styles.searchInput}
                  placeholder="Search for a Physician or Nurse"
                  value={searchText}
                  onChangeText={(text) => setSearchText(text)}
                />
              </View>
              <FlatList
                data={admins.filter((admin) =>
                  admin.displayName.toLowerCase().includes(searchText.toLowerCase())
                )}
                keyExtractor={(admin) => admin.uid}
                renderItem={({ item: admin }) => (
                  <TouchableWithoutFeedback onPress={() => handleAdminClick(admin)}>
                    <View style={styles.userItem}>
                      <Image source={{ uri: admin.photoURL || 'URL_TO_DEFAULT_AVATAR_IMAGE' }} style={styles.userAvatar} />
                      <View>
                        <Text style={styles.userName}>{admin.displayName}</Text>
                        {admin.specialty && admin.specialty.trim() !== '' && (
                          <View style={styles.specialtyBubbles}>
                            <Text style={styles.specialtyText}>{admin.specialty}</Text>
                          </View>
                        )}
                        {admin.lastMessage && ( <Text style={styles.lastMessageText}> {admin.lastMessage.uid === currentUser.uid ?  'You' : 
                                  <Text style={{ fontWeight: 'bold', color: 'black' }}>{admin.lastMessage.displayName}</Text>                            }
                           <Text style={{ color: admin.lastMessage.uid === currentUser.uid ? 'gray' : 'black' }}>:</Text> 
                           {' '}
                           <Text style={{ fontWeight: admin.lastMessage.uid !== currentUser.uid && !isConversationRead ? 'bold' : 'normal', color: admin.lastMessage.uid !== currentUser.uid ? 'black' : 'gray' }}>
                         {admin.lastMessage.text.length > 15 ? admin.lastMessage.text.substring(0, 15) + '...' : admin.lastMessage.text}
                        </Text>
                            {' '}
                            <Text style={{ color: 'gray' }}>
                             {admin.lastMessage.timestamp ? admin.lastMessage.timestamp.toDate().toLocaleTimeString() : ''}
                           </Text>
                            </Text>
                        )}

                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                )}
              />
            </View>
          )}
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  chatContainer: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    margin: 'auto',
    backgroundColor: '#ffffff',
  },
  safeArea: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  title2: {
    fontSize: 32,
    fontWeight: '700',
    marginTop: 25,
    marginBottom: 20,
    color: 'black',
    textAlign: 'left',
  },
  title3: {
    fontSize: 45,
    fontWeight: '700',
    marginTop: 25,
    marginBottom: 20,
    color: 'black',
    textAlign: 'center',
  },
  messageList: {
    flex: 1,
    padding: 10,
  },
  message: {
    backgroundColor: '#e5e5ea',
    padding: 10,
    marginBottom: 10,
    borderRadius: 10,
    width: 'auto',
    alignSelf: 'flex-start',
    maxWidth: '80%',
  },
  sentMessage: {
    alignSelf: 'flex-end',
    backgroundColor: '#DCF8C6',
  },
  receivedMessage: {
    alignSelf: 'flex-start',
    backgroundColor: '#E5E5EA',
  },
  messageHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  messageAvatar: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginRight: 15,
  },
  messageSender: {
    fontWeight: 'bold',
  },
  messageText: {
    textAlign: 'justify',
  },
  messageDetails: {
    fontSize: 12,
    color: '#555',
    marginTop: 5,
  },
  chatInput: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    position: 'absolute',
    bottom: 60,
    left: 0,
    right: 0,
    alignItems: 'center',
    height: 60,
  },
  input: {
    flex: 1,
    padding: 10,
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 10,
    height: '100%',
  },
  button: {
    height: '80%',
    paddingHorizontal: 10,
  },
  keyboardAvoidingContainer: {
    flex: 1,
  },
  specialtyBubble: {
    backgroundColor: '#70bb85',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
  specialtyBubbles: {
  backgroundColor: '#70bb85',
  paddingVertical: 5,
  paddingHorizontal: 10,
  borderRadius: 20,
  marginLeft: 10,
  flexWrap: 'wrap',
  maxWidth: 130, 
},
  specialtyText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  userList: {
    padding: 20,
  },
  userItem: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(173, 216, 230, 0.945)',
    padding: 10,
    marginVertical: 5,
    borderRadius: 10,
  },
  userAvatar: {
    width: 60,
    height: 65,
    borderRadius: 30,
    marginRight: 15,
  },
  userName: {
    fontSize: 18,
    color: '#000',
  },
  backButton: {
    position: 'absolute',
    top: 10,
    left: 10,
    zIndex: 1,
  },
  searchIcon: {
    marginRight: 10,
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#fff',
  },
  searchInput: {
    flex: 1,
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 4,
    padding: 5,
  },
  backIcon: {
    marginRight: 5,
    marginTop: -10,
  },
  lastMessageText: {
    color: '#888',
  },
});

export default ChatScreen;

function getConversationId(uid1, uid2) {
  return [uid1, uid2].sort().join('_');
}
