const symptomToIssueMapping = {
  'Headache': ['Migraine', 'Tension headache', 'Sinus Headache', 'Cluster Headache', 'Temporal Arteritis', 'Trigeminal Neuralgia', 'Ice Cream Headache', 'Thunderclap Headache', 'Hypertension-Related Headache'],
  'Fever': ['Infection', 'Inflammation'],
  'Cough': ['Respiratory tract infection', 'Allergy'],
  'Sore Throat': ['Pharyngitis', 'Tonsillitis'],
  'Dizziness': ['Inner ear infection', 'Dehydration'],
  'Vision Problems': ['Myopia', 'Cataracts'],
  'Nose bleed': ['Nasal trauma', 'Nasal polyps', 'High blood pressure'],
  'Toothache': ['Tooth decay', 'Gum disease', 'Dental abscess'],
  'Tonsillitis': ['Strep throat', 'Viral infection'],
  'Hearing loss': ['Exposure to loud noise', 'Age-related hearing loss', 'Ear infection'],
};

export default symptomToIssueMapping;