import * as React from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { themeColors } from '../theme/index';
import { useNavigation } from '@react-navigation/native';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../config/firebase';

export default function LoginScreen() {
  const navigation = useNavigation();
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [isSecureEntry, setIsSecureEntry] = React.useState(true);

  const handleSubmit = async () => {
    if (email && password) {
      try {
        await signInWithEmailAndPassword(auth, email, password);

        const user = auth.currentUser;
        if (user && !user.emailVerified) {
          // Email is not verified, force signout and show alert
          await auth.signOut();
          alert('Please verify your email before logging in.');
        } else {
          // Email is verified, navigate to the next screen or perform other actions
          
        }
      } catch (err) {
        console.log('got error: ', err.message);
        alert('Login failed. Your email and password do not match.');
      }
    } else {
      if (!email) {
        alert('Please enter your Email Address');
      }
      if (!password) {
        alert('Please enter your Password');
      }
    }
  };

  return (
    <View className="flex-1 bg-white" style={{ backgroundColor: themeColors.secondary }}>
      <SafeAreaView className="flex ">
        <View className="flex-row justify-start" />
        <View className="flex-row justify-center">
          <Image source={require('../assets/images/login.png')} style={{ width: 390, height: 220 }} />
        </View>
      </SafeAreaView>
      <View style={{ borderTopLeftRadius: 50, borderTopRightRadius: 50 }} className="flex-1 bg-lime-100 px-8 pt-8">
        <View className="form space-y-1">
          <Text className="text-gray-700 ml-1">Phinma Email</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-3"
            placeholder="Enter your Email"
            value={email}
            onChangeText={(value) => setEmail(value)}
          />
          <Text className="text-gray-700 ml-1">Password</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl"
            secureTextEntry={isSecureEntry}
            placeholder="Enter your Password"
            value={password}
            onChangeText={(value) => setPassword(value)}
          />
          <TouchableOpacity
            onPress={() => {
              setIsSecureEntry((prev) => !prev);
            }}
            style={{ position: 'absolute', right: 20, top: 135 }}
          >
            <Text>{isSecureEntry ? 'Show' : 'Hide'}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')} className="flex items-end">
            <Text className="text-gray-700 mb-5">Forgot Password?</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={handleSubmit}
            style={{
              padding: 10,
              backgroundColor: '#70bb85',
              borderRadius: 9999,
              overflow: 'hidden',
            }}
          >
            <Text className="text-xl font-bold text-center text-white">Login</Text>
          </TouchableOpacity>
        </View>
        <View className="flex-row justify-center mt-7">
          <Text className="text-gray-500 font-semibold">Don't have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
            <Text style={{ color: '#70bb85', fontWeight: 'bold' }}> Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
