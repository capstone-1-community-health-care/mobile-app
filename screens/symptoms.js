const symptoms = [
  'Headache',
  'Fever',
  'Cough',
  'Sore Throat',
  'Dizziness',
  'Vision Problems',
  'Nose bleed',
  'Toothache',
  'Tonsillitis',
  'Hearing loss',
];
export default symptoms;